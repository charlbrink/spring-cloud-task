package org.bitbucket.charlbrink.cloudtask;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles({"app1, app2"})
public class TaskApplicationTests {

    @Rule
    public OutputCapture outputCapture = new OutputCapture();

    @Test
    public void contextLoads() {
        final String CREATE_TASK_MESSAGE = "Creating: TaskExecution{executionId=";
        final String UPDATE_TASK_MESSAGE = "Updating: TaskExecution with executionId=";
        final String EXIT_CODE_MESSAGE = "with the following {exitCode=0";
        String[] args = {};

        SpringApplication.run(TaskApplication.class, args);

        String output = this.outputCapture.toString();
        assertThat(output.contains(CREATE_TASK_MESSAGE))
                .as("Test results do not show create task message: " + output).isTrue();
        assertThat(output.contains(UPDATE_TASK_MESSAGE))
                .as("Test results do not show success message: " + output).isTrue();
        assertThat(output.contains(EXIT_CODE_MESSAGE))
                .as("Test results have incorrect exit code: " + output).isTrue();

        String taskTitle = "Sample Task";
        Pattern pattern = Pattern.compile(taskTitle);
        Matcher matcher = pattern.matcher(output);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        assertThat(count).as("The number of task titles did not match expected: ")
                .isEqualTo(1);


    }

}
