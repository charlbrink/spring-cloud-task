package org.bitbucket.charlbrink.cloudtask;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@EnableTask
@SpringBootApplication
public class TaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }

    @Bean
    @Profile("app1")
    public TimestampTask1 timeStampTask1() {
        return new TimestampTask1();
    }

    @Bean
    @Profile("app2")
    public TimestampTask2 timeStampTask2() {
        return new TimestampTask2();
    }

    /**
     * A commandline runner that prints a timestamp.
     */
    public static class TimestampTask1 implements CommandLineRunner {
        private String format = "yyyy-MM-dd HH:mm:ss.SSS";

        @Override
        public void run(String... strings) throws Exception {
            DateFormat dateFormat = new SimpleDateFormat(format);
            log.info("Task1 {}", dateFormat.format(new Date()));
        }
    }

    /**
     * A commandline runner that prints a timestamp.
     */
    public static class TimestampTask2 implements CommandLineRunner {
        private String format = "yyyy-MM-dd HH:mm:ss.SSS";

        @Override
        public void run(String... strings) throws Exception {
            DateFormat dateFormat = new SimpleDateFormat(format);
            log.info("Task2 {}", dateFormat.format(new Date()));
        }
    }

}
